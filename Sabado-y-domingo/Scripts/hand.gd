extends Node2D

@onready var open : Sprite2D = $OpenHand
@onready var close : Sprite2D = $ClosedHand


var isClosed : bool = false

var box
var orb

# Called when the node enters the scene tree for the first time.
func _ready():
	DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_HIDDEN)
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position = get_global_mouse_position()
	
	if Input.is_action_pressed("Press"):
		isClosed = true
	else:
		isClosed = false	
	
	if isClosed == true:
		open.visible = false
		close.visible = true
		if box != null && orb == null:
			get_parent().createOrb(box.color)
			orb = get_parent().getOrb()		
	else:
		open.visible = true
		close.visible = false
		if orb != null:
			orb = null
	
	



	
