extends StaticBody2D

@export var color : Global.orbColor
@export var white_box : Texture2D
@export var black_box : Texture2D



# Called when the node enters the scene tree for the first time.
func _ready():
	if color == Global.orbColor.WHITE:
		get_child(0).texture = white_box
	else:
		get_child(0).texture = black_box


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_mouse_entered():
	get_parent().get_node("Hand").box = self


func _on_mouse_exited():
	get_parent().get_node("Hand").box = null
