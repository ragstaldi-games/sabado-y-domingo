extends Area2D

@export var color : Global.orbColor
@onready var outline = $Sprite2D

var hasOrb : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if color == Global.orbColor.BLACK:
		outline.modulate = Color.BLACK
	else:
		outline.modulate = Color.WHITE	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if hasOrb == true:
		monitoring = false


func _on_body_entered(body):
	if body.o_color == color:
		get_tree().root.get_node("Game").addPoints(color)
		body.position = position
		body.rotation = rotation
		hasOrb = true
		outline.visible = false
		
