extends Node

@onready var orbArray = [$OrbPos, $OrbPos2, $OrbPos3, $OrbPos4, $OrbPos5, $OrbPos6, $OrbPos7, $OrbPos8, $OrbPos9, $OrbPos10, $OrbPos11, $OrbPos12 ]
@onready var dialog : AudioStreamPlayer2D = $Dialog


@export var endLine : AudioStreamWAV

var orb = preload("res://Nodes/orb.tscn")
var player = preload("res://Nodes/player.tscn")

var currentOrb

var canPlay : bool = true

var whites : int
var blacks : int

var totalScore

# Called when the node enters the scene tree for the first time.
func _ready():
	for n in orbArray:
		n.visible = false
		n.monitoring = false
	
	dialog.play()	



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	totalScore = whites + blacks
	if totalScore < 12:
		orbArray[totalScore].visible = true
		orbArray[totalScore].monitoring = true
	
	

func createOrb(color : Global.orbColor):
	currentOrb = orb.instantiate()	
	add_child(currentOrb)
	currentOrb.setOrb(color)

func addPoints(c : Global.orbColor):
	if c == Global.orbColor.WHITE:
		whites += 1
	else:
		blacks += 1
	get_node("Hand").orb = null		
	
func getOrb() -> RigidBody2D:
	return currentOrb	


func _on_dialog_finished():
	if totalScore >= 12:
		var instance = player.instantiate()
		add_child(instance)
		instance.get_child(0).finished.connect(_on_video_stream_player_finished)
		instance.get_child(0).play()


func _on_video_stream_player_finished():
	get_tree().quit()
