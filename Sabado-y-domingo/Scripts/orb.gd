extends RigidBody2D


var o_color : Global.orbColor

@onready var blackOrb : Sprite2D = $Black
@onready var whiteOrb : Sprite2D = $White

var placed : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_parent().get_node("Hand").isClosed == true && get_parent().get_node("Hand").orb == self:
		position = get_global_mouse_position()
		

func setOrb(orb : Global.orbColor):
	o_color = orb
	
	if o_color == Global.orbColor.BLACK:
		blackOrb.visible = true
	else:
		whiteOrb.visible = true
	



